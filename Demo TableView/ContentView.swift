//
//  ContentView.swift
//  Demo TableView
//
//  Created by Dmnhat on 9/8/20.
//  Copyright © 2020 Dmnhat. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
