//
//  ContactsDetailViewController.swift
//  Demo TableView
//
//  Created by Dmnhat on 9/8/20.
//  Copyright © 2020 Dmnhat. All rights reserved.
//

import UIKit

class ContactsDetailViewController: UIViewController {

    var contactName = ""
    
    @IBOutlet weak var lbContactName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        lbContactName.text = contactName
        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
