//
//  ContactsViewController.swift
//  Demo TableView
//
//  Created by Dmnhat on 9/8/20.
//  Copyright © 2020 Dmnhat. All rights reserved.
//

import UIKit

class ContactsViewController: UIViewController {
    
    
    //Create a String array
    var contacts: [String] = []
    

    @IBOutlet weak var contactTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Contacts"
        loadData()
        configTableView()
 
    }
    
    func loadData() {
        guard let path = Bundle.main.url(forResource: "Contacts", withExtension: "plist")
            else {return}
        guard let contactsData = NSArray(contentsOf: path) as? [String]
            else {
                return
        }
        
        contacts = contactsData
    }
    
    func configTableView() {
        contactTableView.register(UITableViewCell.self, forCellReuseIdentifier: "UITableView")
        contactTableView.dataSource = self
        contactTableView.delegate = self
    }
    
 

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension ContactsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = contactTableView.dequeueReusableCell(withIdentifier: "UITableView", for: indexPath)
        cell.textLabel?.text = contacts[indexPath.row]
        return cell
    }

}

extension ContactsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let contactDetailViewController = ContactsDetailViewController()
        contactDetailViewController.contactName = contacts[indexPath.row]
        navigationController?.pushViewController(contactDetailViewController, animated: true)
    }
}
