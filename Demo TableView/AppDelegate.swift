//
//  AppDelegate.swift
//  Demo TableView
//
//  Created by Dmnhat on 9/8/20.
//  Copyright © 2020 Dmnhat. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
       
        let contactViewController = ContactsViewController()
        
        let navigationController = UINavigationController(rootViewController: contactViewController)
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navigationController
        window?.backgroundColor = .white
        window?.makeKeyAndVisible()
        return true
    }

   
}

